---
title: "Objects"
---

## A Little Story

Let's imagine that we analyze gene data of wild animals for our research.
To do so, we have a huge amount of samples from various specimens that need to be tracked, catalogued and analyzed.

We will start out simple and track the ID numbers of each sample.
A list will do the job fine.

```python3
# I am making up a naming pattern here, use your own if you like
sample_identifiers = ["0123", "0124", "0120a", "0120b"]
```

To give proper credit, we should also track who collected the samples.
So… how about a second list?

```python3
sample_collectors = ["Darwin", "Mendel", "Darwin", "Irwin"]
```

> Do [Task 1](../exercises/task-01/task-01.md) now

## Introducing Objects

We obviously need a better way to model more complex data.
For that purpose we introduce the idea of an **object**.
An object represents an amount of data that belongs together in the context of a program.
In our example each collected sample could be represented by an object:

```nomnoml
[<instance> sample 0 |
identifier = "0123"
collector = "Darwin"
]

[<instance> sample 1 |
identifier = "0124"
collector = "Mendel"
]

[<instance> sample 2 |
identifier = "0120a"
collector = "Darwin"
]

[<instance> sample 3 |
identifier = "0120b"
collector = "Irwin"
]
```
