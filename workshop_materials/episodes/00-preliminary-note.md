---
title: "Introduction"
---

## Preliminary Note

Learning OOP can be tricky for three reasons:

1. It is a very different concept from the _imperative programming_ that usually is taught to beginners.
2. A lot of problems and descriptions are given in natural language with all the ambiguity and implicit information that comes with it.
   This can make understanding the issues to be solved a lot more difficult.
3. It introduces a lot of technical terms with very specific meaning and its own graphical notation (called UML).

Learners are advised to make a concious effort to correctly use these terms for themselves as well to become more familiar with them.
There is a [glossary](../resources/glossary.md) included in the workshop material to help with orientation.
On the side of graphical notation a reduced version will be used, which omits details not relevant for the context.
It is not the focus of this workshop to teach this notation.
Using the reduced version of UML aids in the understanding of complex situations and facilitates an initial acquaintance with this style of representing code.
