---
title: "Classes"
---

## A more generalized approach

Each of these samples has the same kind of information attached to it, so we can describe what a sample in general looks like:
We can summarize these common features as: "Every sample has an _identifier_ and a _collector_".

This abstraction is called a **class**.
The "variables" that each object of the same class has are called **attributes**.
In our case these are _identifier_ and _collector_.

```nomnoml
[Sample |
    identifier
    collector
|]
```

An object that is of a certain class is called an **instance** of that class.
**Instantiation** thus is the process of creating a new object from a class.

Let us see how to write this down in Python, step-by-step:

```python
# Here is the most minimal class
class Sample:
    pass  # Tell Python that this is all (for now)

my_sample = Sample()  # Create a new instance of Sample
my_sample.identifier = "0123"  # Set the attributes
my_sample.collector = "Darwin"

print("Object:", my_sample)
print("ID:", my_sample.identifier)
print("Collected by:", my_sample.collector)
```

Note how the output of printing an object directly is pretty awkward.
We will learn how to improve that later.

There is one further important insight: **Classes are data types**.
Try:

```python
my_sample = Sample()
type(my_sample)
```
This means that you can use any object as a value for variables or put them into a function as values for parameters.
