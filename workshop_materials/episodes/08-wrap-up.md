---
title: "Wrap-up"
---

## Where we are

We now have implemented a small model of the elements in our sample analysis process that we can use as the base for adding further details.
Here is how it currently looks:

```nomnoml
[ Sample |
    identifier
    collector
    found_at
    analysis_done
    dna_sequence
|
    __init__(self, identifier, collector, location, found_at)
    __str__(self)
    take_analysis_result(self, dna_sequence)
]

[ Location |
    longitude
    latitude
|
    __init__(self, longitude, latitude)
    normalize(self, value, limit)
]

[ <abstract> DnaSequencer |
    BASES = "\[A, C, G, T\]"
    MODEL
    SEQUENCE_LENGTH
    serial_number
    is_clean
|
    __init__(self, serial_number, is_clean)
    clean(self)
    analyze_sample(self, sample)
    verify_dna_sequence(cls, dna_sequence)
]

[ DnaMaster2000 |
    MODEL = "DNA Master 2000"
    SEQUENCE_LENGTH = 2000
|
    __init__(self, serial_number)
    analyze_sample(self, sample)
]

[ DnaMaster3000 |
    MODEL = "DNA Master 3000"
    SEQUENCE_LENGTH = 3000
|
    __init__(self, serial_number)
    analyze_sample(self, sample)
    run_demonstration(self, sample)
]

[<label> found_at]
[<label> analyzes]

[Sample] o- [found_at]
[found_at] - [Location]

[DnaSequencer] <:- [DnaMaster2000]
[DnaSequencer] <:- [DnaMaster3000]

[analyzes] - [DnaSequencer] 
[Sample] <- [analyzes]
```

This model by itself does nothing.
Each OOP program also needs some code that sets up the model elements and triggers their interactions.
The benefit of having a model is that it can be re-used as long as it has the elements to describe the setup you want to simulate or analyze.

> Exercise what we have learned with
  [Task 9](../exercises/task-09/task-09.md),
  [Task 10](../exercises/task-10/task-10.md),
  [Task 11](../exercises/task-11/task-11.md) and
  [Task 12](../exercises/task-12/task-12.md)
