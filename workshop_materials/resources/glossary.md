---
title: Glossary
---

# Glossary

This is a collection of the technical terms that are introduced alongside object-oriented programming.

## Attribute

A variable-like [member](#member) of a [class](#class).
Attributes define which kinds of data are stored within a class.

??? info "Instance Attributes"
    These are attributes that belong directly to objects.
    They are defined inside the [constructor](#constructor).

??? info "Class Attributes"
    These are attributes which belong directly to the class and are defined on the class level.
    All instances of a class share these same attributes.

---

## Class

Each class is a definition of a data format.
It consists of members which can be [attributes](#attribute) or [methods](#method).
Each class is by definition a data type.
Classes can be combined by using [composition](#composition) and extended and modified via [inheritance](#inheritance)

---

## Composition

A class `Ship` is composed of the class `SteamTurbine`, if an attribute `engine` of the `Ship` is of the type `SteamTurbine`.

```mermaid
classDiagram
    direction LR

Ship *-- SteamTurbine : engine

Ship:engine
```

In natural language we express this as 
> A `Ship` _has_ a `SteamTurbine` as an `engine`.

---

## Constructor

A special [method](#method) that is called when an [object](#object) is [instantiated](#instance) from a [class](#class).
In Python its signature is `__init__(self, …)`.

---

## Inheritance

If a class `Car` inherits from `MotorVehicle` it takes over all pre-existing attributes and methods of the parent class.
It can replace (_override_) the inherited methods with its own implementation.
The inheriting class can add its own members as well. (e.g. a `Car` may additionally have a `trunk`)

```mermaid
classDiagram
    direction LR

class MotorVehicle {
    motor
    start_motor()
}

class Car {
    motor
    trunk
    start_motor()
}

MotorVehicle <|-- Car
```

In natural language we say
> A `Car` _is_ a `MotorVehicle`.

??? info "Multiple Inheritance"
    In _Python_ a class may have any amount of parent classes.
    This is called _multiple inheritance_.
    For example a `Train` can inherit from `MotorVehicle` and `RailVehicle`.
    Multiple inheritance can be very useful, but also has its very own caveats, that can be hard to debug.
    
    ```mermaid
    classDiagram
        direction LR

    class MotorVehicle {
        motor
        start_motor()
    }

    class RailVehicle {
        rail_gauge
    }

    class Train {
        motor
        rail_gauge
        start_motor()
    }

    MotorVehicle <|-- Train
    RailVehicle <|-- Train
    ```

??? info "Super- and Sub-Classes"
    A class that is more general is called a **Super-class** of a more specific one, which in turn is a **Sub-class**.
    For example:
    
    * `MotorVehicle` is a super-class of `Car`
    * `Train` is a sub-class of `RailVehicle`

---

## Instance

An [object](#object) is considered to be an instance of a [class](#class) if the class (or one of its sub-classes) is the data type of the object.
Objects get created from classes during the _instantiation_ which involves the [constructor](#constructor)-call.

---

## Member

The generic term for the components of a class, mostly refers to [attributes](#attribute) and [methods](#method).

---

## Method

A function-like construct in the context of a [class](#class).
By default it operates on a specific [object](#object) for which the method is called.
Methods that operate on a class itself are called **class methods**.
Methods that are [members](#member) of a class but do neither require an object nor the class itself for context are called **static methods**.

---

## Object

A concrete [instance](#instance) of a [class](#class) during the run of a program.
The data values contained in an object are called [instance attributes](#attribute).
