# Workshop - OOP in Python

This is the writeup for a workshop to introduce Object-oriented programming in Python.
The contents is divided in _episodes_ and _exercises_.
In the episodes it is pointed out which exercise would be suitable at which stage of progress to keep the learning and storytelling flow.

## Audience and Prerequisites
This workshop is intended for learners who have a basic understanding of working with Python like

* Variables, data types, functions
* Loops, conditionals

but may not yet have worked with any object-oriented language.

You should be able to work with a _Python_ tool of your choice (e.g. on the command line, via _Spyder_, _Jupyter_, _Pycharm_ or any other) and know how to write and run Python modules (i.e. files with Python code in it).

## Structure

The content is split into thematic episodes which are ordered along a story arc.
The episodes point out when to do which exercise.

The exercises themselves are split into Tasks which contain the task to be solved and a code checkpoint with the state of the workshop project before the task to make sure learners start out from the same point.

## Contact

For any inquiries about this workshop please contact [the HIFIS support](mailto:support@hifis.net)
