
# Here is the most minimal class
class Sample:

    # Tell python how to construct an object of that class
    def __init__(self, identifier, collector):
        self.identifier = identifier
        self.collector = collector
        self.analysis_done = False
        self.dna_sequence = None

    # This will help us with printing the instances in a nice fashion
    def __str__(self):
        text = "Sample: " + self.identifier + ", collected by " + self.collector
        if self.analysis_done:
            text = text + " (analyzed)"
        return text

    def take_analysis_result(self, dna_sequence):
        self.dna_sequence = dna_sequence
        self.analysis_done = True
