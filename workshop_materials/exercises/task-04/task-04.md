---
title: "Task 04"
---

??? Checkpoint
    This is the state of the project so far

    * [sample.py](checkpoint_before/sample.py)

## Task 4: Location, location, location

For our later analysis we should also keep track where a given sample was found.
Create a class `Location` in its own file `location.py`.
It has a `longitude` and a `latitude` (in degrees) as attributes, which will be provided on instantiation.

Make sure that the input for the location is within to the ranges `-90° ≤ latitude ≤ 90°` and `-180° < longitude ≤ 180°`.

A higher / lower value for the latitude shall be clamped to the nearest limit.
**Example:** A latitude of 120° is given and will be clamped to 90°. A latitude of -1000° is given and will be clamped to -90°.

A higher/lower value of the longitude shall be corrected by the formula:
```
new_longitude = ((longitude - 180) % -360) + 180
```
Create the methods `normalize_latitude(…)` and `normalize_longitude(…)` to perform necessary checks and corrections.
