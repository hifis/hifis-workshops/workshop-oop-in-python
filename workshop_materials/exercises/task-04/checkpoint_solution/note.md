# Note on the Suggested Solution
The `normalize…`-methods do not need neccessarily need to be member methods,
since they do not use the `self` reference in any way.
It is done this way because we have not learned about _static methods_ yet.

An alternative solution would be to assign the values in the `__init__` first.
Then call the `normalize…`-methods with only the `self` parameter, and use side
effects to correct the values.
