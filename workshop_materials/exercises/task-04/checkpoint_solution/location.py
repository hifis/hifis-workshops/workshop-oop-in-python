
class Location:
    def __init__(self, longitude, latitude):
        self.longitude = self.normalize_longitude(longitude)
        self.latitude = self.normalize_latitude(latitude)

    def normalize_latitude(self, latitude):
        if latitude > 90:
            return 90
        if latitude < -90:
            return -90
        return latitude

    def normalize_longitude(self, longitude):
        return ((longitude - 180) % -360) + 180
 
