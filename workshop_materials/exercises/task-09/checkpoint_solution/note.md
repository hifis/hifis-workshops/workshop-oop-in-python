
## Suggested Solution

It is suggested to store the information in the `Sample` class as a new attribute.
The method `take_analysis_result(…)` should be extended to accept the serial number of the DnaSequencer as a parameter.
In conjunction, the `analyze_sample(…)`-method must be changed to also pass in the serial number when calling `take_analysis_result(…)`.
Since all subclasses of `DnaSequencer` call upon the superclass implementation, there is no need to actually change them.
