---
title: "Task 06"
---

??? Checkpoint
    This is the state of the project so far

    * [sample.py](checkpoint_before/sample.py)
    * [location.py](checkpoint_before/location.py)
    * [dna_sequencer.py](checkpoint_before/dna_sequencer.py)

## Task 6: Training makes perfect

Introduce a **class attribute** `SEQUENCE_LENGTH` which encodes the technical limitation of a `DnaSequencer`.
Set this to have a value of 2000.

Use it as a default sequence length in the `analyze_sample(…)`-method.
