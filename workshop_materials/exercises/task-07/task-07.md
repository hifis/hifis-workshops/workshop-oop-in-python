---
title: "Task 07"
---

??? Checkpoint
    This is the state of the project so far

    * [sample.py](checkpoint_before/sample.py)
    * [location.py](checkpoint_before/location.py)
    * [dna_sequencer.py](checkpoint_before/dna_sequencer.py)

## Task 07: A new Normal

In addition to _class methods_ there are _static methods_ that are part of a class but do not even require the class itself to be a parameter.
They could be stand-alone functions and are usually part of classes to keep the code organized.
To mark a static method, we can use the annotation `@staticmethod`
One example of those are the `normalize…`-methods in the `Location`-class.

Turn the `normalize…`-methods in the `Location`-class into static methods. Note that static methods can be called upon instances or classes.
Should the call of these methods in the constructor change?
