
# Here is the most minimal class
class Sample:

    # Tell python how to construct an object of that class
    def __init__(self, identifier, collector):
        self.identifier = identifier
        self.collector = collector

    # This will help us with printing the instances in a nice fashion
    def __str__(self):
        return "Sample: " + self.identifier + ", collected by " + self.collector
