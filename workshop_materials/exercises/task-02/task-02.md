---
title: "Task 02"
---

??? Checkpoint
    This is the state of the project so far

    * [sample.py](checkpoint_before/sample.py)

## Task 2

Our `Sample` class is looking pretty nice, but it is not really doing a lot, is it?
In addition to the identifier and the collector let us track if a sample has been analyzed yet.

### Task 2a: Add an attribute

Extend the class by an attribute called `analysis_done`.
This attribute shall track whether a sample has already been analyzed.
Its value can be either `True` or `False`.
Of course when the sample is newly registered (i.e. a `Sample`-instance is created) it has not been analyzed yet.

### Task 2b: Include the new attribute in the `__str__`-method

When  printing a `Sample`-instance, add the text `(analyzed)` at the end of the output if the sample has been analyzed.

**Note:**
This will likely require you to introduce intermediate steps before you get the final result.
There are multiple solutions that are equally possible and valid.
