---
title: "Task 01"
---

??? Checkpoint
    This is the state of the project so far

    * [sample.py](checkpoint_before/sample.py)


## Task 1: What could go wrong?

Discuss in small groups:

* Which issues could arise if data that inherently belongs together is kept in separate variables?
* Which alternatives can you imagine to fix these issues?
  Do these have other drawbacks?

Also consider which challenges could arise in the future if new data is to be added into the program (e.g. the time the sample was taken)
