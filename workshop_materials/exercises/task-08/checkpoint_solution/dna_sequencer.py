from random import choices

class DnaSequencer:

    BASES = ["A", "C", "G", "T"]
    SEQUENCE_LENGTH = 2000
    # If it is written at the indentation level
    # directly below the class header, it is a class attribute
    # This time in upper case because we want this to be treated
    # as a constant, which are written this way by convention

    def __init__(self, serial_number, is_clean = False):
        self.serial_number = serial_number
        self.is_clean = is_clean

    def clean(self):
        self.is_clean = True
        print("DNA sequencer", self.serial_number, "has been cleaned")

    def analyze_sample(self, sample):
        if not self.is_clean:
            print("Sequencer", self.serial_number, "is still dirty - abort")
            return

        dna_sequence = "".join(
            choices(
                DnaSequencer.BASES,
                k=DnaSequencer.SEQUENCE_LENGTH
            )
        )
        sample.take_analysis_result(dna_sequence)

        # Analysis done, device got dirty
        self.is_clean = False

    @classmethod
    def verify_dna_sequence(cls, dna_sequence):
        if len(dna_sequence) != cls.SEQUENCE_LENGTH:
            return False  # Sequence is too long or short
        for base in dna_sequence:
            if base not in cls.BASES:
                return False  # Encountered a symbol in the sequence that doesn't belong there
        return True  # None of the previous checks failed, should be fine

class DnaMaster2000(DnaSequencer):
    MODEL = "DNA Master 2000"

    def __init__(self, serial_number):
        super().__init__(
            serial_number=serial_number,
            is_clean=True  # Comes in a sterile packaging
        )

    def analyze_sample(self, sample):  # Provide our own implementation
        print(self.MODEL, "- S/N", self.serial_number)
        super().analyze_sample(sample)  # Call to the superclass implementation
        print("Analyzed ", len(sample.dna_sequence), "bases")

class DnaMaster3000(DnaSequencer):
    MODEL = "Dna Master 3000"
    SEQUENCE_LENGTH = 3000

    def __init__(self, serial_number):
        super().__init__(
            serial_number=serial_number
        )

    def analyze_sample(self, sample):
        print(self.MODEL, "- S/N", self.serial_number)
        print("Analyzing sample", sample)
        super().analyze_sample(sample)

        if sample.dna_sequence:
            print("Preview:", sample.dna_sequence[:20])
        else:
            print("No preview available")

    def run_demonstration(self, sample):
        print(self.model, "- S/N", self.serial_number)
        print("Analyzing sample", sample)
        print("Preview:", "ACGT" * 5)
