---
title: "Task 08"
---

??? Checkpoint
    This is the state of the project so far

    * [sample.py](checkpoint_before/sample.py)
    * [location.py](checkpoint_before/location.py)
    * [dna_sequencer.py](checkpoint_before/dna_sequencer.py)

## Task 8: Upgrade

We now equip our laboratory with a new DNA sequencer.
The _DNA Master 3000_ has a few new gimmicks:

* It produces sequences with a length of 3000.
* When analyzing, it
    1. Prints the model and serial number (like the 2000-variant)
    2. Prints the sample it is processing
    3. Does the analysis
    4. Outputs a preview with the first 20 bases of the sequence it produced (only if the analysis added a DNA sequence to the sample)
* This new model has a functionality `run_demonstration(…)` which acts as if analyzing a sample, but does not actually modify the sample and uses the fixed sequence `ACGT` repeated 5 times.
* Unlike the _2000_ model it does not always arrive in a clean state

Create a new (sub-) class for this device.
From which class should you inherit, if any?
Which methods do need to be overridden?
