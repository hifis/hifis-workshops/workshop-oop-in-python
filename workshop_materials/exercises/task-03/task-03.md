---
title: "Task 03"
---

??? Checkpoint
    This is the state of the project so far

    * [sample.py](checkpoint_before/sample.py)

## Task 3

The samples we collected in the wild can be analyzed to extract DNA sequences from them.

### Task 3a: Add another attribute

Extend the `Sample`-class by adding an attribute called `dna_sequence`.
Its initial value is `None`, since we have not run an analysis yet.

### Task 3b: Thinking ahead

It is suggested to later store the DNA sequence as a string made up from the letters _A_, _C_, _G_, _T_ for now. Discuss potential advantages and disadvantages of this approach with respect to potential future use cases.

### Task 3c: Getting Results

For later use we want a method to pass analysis results into the `Sample`-class.
Write a method `take_analysis_result(…)`, that takes a parameter `dna_sequence` and passes it on to the attribute with the same name.
Further, it also should set the attribute `analysis_done` to `True`.

