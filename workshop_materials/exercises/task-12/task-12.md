---
title: "Task 12"
---

## 12: A complete experiment

Create a new class `Experiment`.
An experiment is _composed from_ a specific `DnaSequencer` (e.g. the `DnaMaster2000`) and a `RnaSequencer`.
Each `Experiment` has a method `run(self, sample)` which does the following steps:

1. Input the sample to the `DnaSequencer`
2. Check if the sample now contains a DNA sequence. If not, print an error and abort the run.
3. Input the sample to the `RnaSequencer`
4. Check if the triplet `"UAC"` is now present in the sample. If not, the sample may have been analyzed wrong. Print an error and restart at step 1.
5. Report that the sample was analyzed successfully and print the first 5 triplets.

Test your experiment with a few samples.
