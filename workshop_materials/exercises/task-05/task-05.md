---
title: "Task 05"
---

??? Checkpoint
    This is the state of the project so far

    * [sample.py](checkpoint_before/sample.py)
    * [location.py](checkpoint_before/location.py)

## Task 5: A class of its own (again)

### Task 5a: Baby Steps

Create a class to represent a `DnaSequencer`.
Everything related to DNA sequencers should go into the file `dna_sequencer.py`
It has an attribute to represent its `serial_number`(for bookkeeping), that get set upon instantiation.
Further the device keeps track if it `is_clean`, which is also to be determined on instantiation.
If not given otherwise, a DNA sequencer will assumed to be not clean after delivery, just to be sure.

Add a method `clean()`, that resets the `is_clean`-attribute to `True`.
Print some output to indicate that the sequencer is now clean. Include the serial number in the output the sequencers can be identified.

### Task 5b: Simulated Sequencing

While in reality DNA sequencing is an elaborate and complicated process with a lot of code involved to control all the machines, for brevity, we will use a short snippet of Python code as a placeholder:

```python
# We use a random generator to make up the analysis result
from random import choices

# DNA is made up of "bases", which can be represented by one of the following letters
bases = ["A", "C", "G", "T"]
sequence_length = 2000  # How many bases the generated DNA sequence should have

# Create a string made up from a random selection of bases
# The following construction is a little bit tricky to understand,
# Can you figure it out?
dna_sequence = "".join(choices(bases, k=sequence_length))
```

Expand the `DnaSequencer` by a method `analyze_sample`.
This method takes a `Sample`-instance as a parameter (called `sample`).
If the sequencer is not clean, print a warning message and do nothing else.
Otherwise, it internally generates a new DNA sequence based on the code shown above and uses the `take_analysis_result`-method of the parameter to hand over the generated sequence.
After generating a sequence, the device is dirty, so set the attribute accordingly.

You can use the following code to test your implementation:

```python
# Do not forget the imports here as needed:
import …

my_sample = Sample(identifier="0123", collector="Darwin", found_at=location.NAURU)
my_sequencer = DnaSequencer(serial_number="R2D2-C3PO")
my_sequencer.clean()  # Make sure to not try to run the analysis wit dirty equipment…
my_sequencer.analyze_sample(sample=my_sample)

# Let's see if it worked
print(my_sample)  # should say "(analyzed)"
print(my_sample.dna_sequence)  # Should be a 2000-letter DNA sequence

my_other_sample = Sample(identifier="0124", collector="Mendel", found_at=location.DRESDEN)
# The sequencer should be not clean by now, let's see what happens
my_sequencer.analyze_sample(sample=my_other_sample)

```
