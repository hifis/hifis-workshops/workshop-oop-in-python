
## Suggested Solution

```python
class Sample:
    DNA_INVERSE = {"A": "T", "C": "G", "G": "C", "T": "A"}

    …  # Everything else as before
    
    def template_strand(self):
        return self.dna_sequence
        
    def coding_strand(self):
        if not self.dna_sequence:   # Throwing an error would be better
            return None             # But we have not learned that yet
        coding_strand = ""
        for nucleotide in self.dna_sequence:
            coding_strand += Sample.DNA_INVERSE[nucleotide]
        return coding_strand
```

> A slightly more clean approach to implementing the `template_strand(…)`- and `coding_strand(…)`-methods would be to implement them as so called **properties**, since they both refer to the same attribute. [Learn more about properties][python-properties]

[python-properties]: https://realpython.com/python-property/
