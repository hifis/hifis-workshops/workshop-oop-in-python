
# OOP Elements

These are the puzzle pieces out of which an object-oriented program is composed and their relations with each other in one handy diagram.

```mermaid
classDiagram
    
    class FunctionLike
    <<abstract>> FunctionLike
    
    class VariableLike
    <<abstract>> VariableLike
    
    Class <|.. Object : instance
    Class o-- FunctionLike :members
    Object o-- InstanceAttribute
    Attribute <|.. InstanceAttribute
    FunctionLike <|-- Method
    FunctionLike <|-- StaticMethod
    FunctionLike <|-- ClassMethod
    Object .. Method : self
    Class .. ClassMethod: cls
    Class o-- VariableLike : members
    VariableLike <|-- Attribute
    VariableLike <|-- ClassAttribute

```
