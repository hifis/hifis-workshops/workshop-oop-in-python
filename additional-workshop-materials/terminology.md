## Terminology

#### Class
Definition of a data format.
It consists of **members** which can be **attributes** or **methods**.
_Each class is by definition a data type._

#### Attributes
A variable-like **member** of a **class**.
Attributes define which kinds of data are stored within a class.
They are defined inside the **constructor**.

#### Object
A concrete **instance** of a **class** during the run of a program.
The data values contained in an object are called **instance attributes**.

#### Method
A function-like construct in the context of a **class**.
It by default operates on a specific **object** for which the method is called.
Methods that operate on a class itself are called **class methods**.
Methods that are **members** of a class but do neither require an object nor the class itself for context are called **static methods**.

#### Constructor
A special **method** that is called when an **object** is instantiated from a **class**.
In Python its signature is `__init__(self, …)`.

#### Composition
A class `Car` is composed of the class `DieselMotor`, if an attribute `engine` of `Car` is of the type `DieselMotor`.


```mermaid
classDiagram
    direction LR

Car *-- DieselMotor : engine

Car:engine
```

In natural language we express this as 
> A `Car` _has_ a `DieselMotor` as an `engine`.

#### Inheritance
If a class `Car` inherits from `MotorVehicle` it takes over all pre-existing attributes and methods of the parent class.
In _Python_ a class may have any amount of parent classes.
This is called _multiple inheritance_.
For example a `Train` can inherit from `MotorVehicle` and `RailVehicle`.
:warning: Multiple inheritance can be very useful, but also has its very own caveats, that can be hard to debug.

```mermaid
classDiagram
    direction LR

class MotorVehicle {
    motor
    drive()
}

class RailVehicle {
    gauge
}

class Car {
    motor
    drive()
}

class Train {
    motor
    gauge
    drive()
}

MotorVehicle <|-- Car
MotorVehicle <|-- Train
RailVehicle <|-- Train

```

In natural language we say
> A `Car` _is_ a `MotorVehicle`.

A class that is more general is called a **Super-class** of a less general one, which in turn is a **Sub-class**.
For example:
* `MotorVehicle` is a super-class of `Car`
* `Train` is a sub-class of `RailVehicle`

:::info
If faced with a complex design situation:
**Composition beats inheritance** when concerning attributes.
Inheritance is primary used to transfer behaviour.
Which one to use in which situation can be a tough question even for experts.
::: 
